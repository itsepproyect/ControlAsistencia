CREATE TABLE IF NOT EXISTS public.registros (
  registro timestamp without time zone,
  id_user integer,
  id INT AUTO_INCREMENT PRIMARY KEY,
  CONSTRAINT registros_pkey PRIMARY KEY (id)
);

INSERT INTO public.registros (registro, id_user) VALUES
  ('2023-04-26 17:35:01.96327', 102),
  ('2023-04-26 18:29:08.838561', 23),
  ('2023-04-26 18:48:21.199837', 102),
  ('2023-04-26 19:16:06.748825', 44),
  ('2023-04-26 19:16:59.045552', 10),
  ('2023-04-27 20:35:59.045552', 102),
  ('2023-04-28 07:15:59.045552', 102);
