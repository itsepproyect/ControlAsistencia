CREATE TABLE IF NOT EXISTS public.users
(
    id INT AUTO_INCREMENT PRIMARY KEY,
    password VARCHAR(50) NOT NULL,
    name VARCHAR(50) NOT NULL,
    typeUser VARCHAR NOT NULL
);


INSERT INTO users(password, name, typeUser) VALUES
('lwrTSIFX', 'Brita', 'ADMINISTRATOR'),
('aAIFo3chyndW', 'Bitchip', 'ADMINISTRATOR'),
('8JRrupUdxvt9', 'Konklux', 'ADMINISTRATOR'),
('ineTlN5c1tVB', 'Bytecard', 'STUDENT'),
('RIlHA59', 'Zathin', 'TEACHER'),
('xwRFmdx6ec4', 'Prodder', 'STUDENT'),
('17hIgep', 'Keylex', 'TEACHER'),
('uVJ2YP', 'Holdlamis', 'ADMINISTRATOR'),
('ePAZHvf', 'Wrapsafe', 'ADMINISTRATOR'),
('tvSodFNrsQy0', 'Asoka', 'STUDENT'),
('SLTSeN8scOF', 'It', 'STUDENT');