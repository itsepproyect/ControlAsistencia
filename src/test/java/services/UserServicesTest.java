package services;

import data.RegistersRepositoryDAO;
import data.UserRepositoryDAO;
import models.TypeUser;
import models.User;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.sql.DataSource;

import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserServicesTest {

    UserServices userServices;

    private DataSource dataSourse;

    RegistersRepositoryDAO registersRepositoryDAO;
    UserRepositoryDAO userRepositoryDAO;

    @Before
    public void setUp() throws Exception {
        //Ojo que aqui creamos una base de datos en memoria de prueba, esta guay
        dataSourse = new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa","sa");
        //Se ejecuta un script para que un archivo en resources en test con el codigo SQL cree la base de datos con algunos usuarios dentro
        ScriptUtils.executeSqlScript(
                dataSourse.getConnection(), new ClassPathResource("sql-scripts/users-test-data.sql")
        );
        //El template que necesita mi repositorio para conectarse a la base de datos
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourse);

        userRepositoryDAO = new UserRepositoryDAO(jdbcTemplate);
        registersRepositoryDAO = new RegistersRepositoryDAO(jdbcTemplate);

        userServices = new UserServices(
                userRepositoryDAO, registersRepositoryDAO
        );

    }

    @Test
    public void login_user() {
        User userLogging = new User(10, "tvSodFNrsQy0");
        assertTrue(userServices.loginUser(userLogging));
    }
    @Test
    public void get_user_by_id() {
        User userLogged = userServices.getUserById(10);

        List<Integer> usersIds = Collections.singletonList(userLogged.getId());

        assertThat(usersIds, CoreMatchers.is(Arrays.asList(10)));
    }

    @Test
    public void get_user_by_typeUser() {
        Collection<User> users = userServices.getUsersByTypeUser(TypeUser.STUDENT);

        List<Integer> userIds = users.stream().map(User::getId).collect(Collectors.toList());

        assertThat(userIds, CoreMatchers.is(Arrays.asList(4, 6, 10, 11)));
    }

    @After
    public void tearDown() throws Exception {

        // Remove H2 files -- https://stackoverflow.com/a/51809831/1121497
        final Statement s = dataSourse.getConnection().createStatement();
        s.execute("drop all objects delete files"); // "shutdown" is also enough for mem db
    }

}