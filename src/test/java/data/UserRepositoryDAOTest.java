package data;

import models.User;
import models.TypeUser;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.sql.DataSource;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserRepositoryDAOTest {

    private UserRepositoryDAO userRepository;
    private DataSource dataSourse;
    @Before
    public void setUp() throws Exception {
        //Ojo que aqui creamos una base de datos en memoria de prueba, esta guay
        dataSourse = new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa","sa");
        //Se ejecuta un script para que un archivo en resources en test con el codigo SQL cree la base de datos con algunos usuarios dentro
        ScriptUtils.executeSqlScript(
                dataSourse.getConnection(), new ClassPathResource("sql-scripts/users-test-data.sql")
        );
        //El template que necesita mi repositorio para conectarse a la base de datos
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSourse);
        //Resposirotio que necesita a la base de datos
        userRepository = new UserRepositoryDAO(jdbcTemplate);
    }

    @Test
    public void get_all_users() {
        Collection<User> users = userRepository.findAll();

        assertThat(users, CoreMatchers.is(Arrays.asList(
                new User(1, "lwrTSIFX", "Brita", TypeUser.ADMINISTRATOR),
                new User (2 ,"aAIFo3chyndW", "Bitchip", TypeUser.ADMINISTRATOR),
                new User (3 ,"8JRrupUdxvt9", "Konklux", TypeUser.ADMINISTRATOR),
                new User (4 ,"ineTlN5c1tVB", "Bytecard", TypeUser.STUDENT),
                new User (5 ,"RIlHA59", "Zathin", TypeUser.TEACHER),
                new User (6 ,"xwRFmdx6ec4", "Prodder", TypeUser.STUDENT),
                new User (7 ,"17hIgep", "Keylex", TypeUser.TEACHER),
                new User (8 ,"uVJ2YP", "Holdlamis", TypeUser.ADMINISTRATOR),
                new User (9 ,"ePAZHvf", "Wrapsafe", TypeUser.ADMINISTRATOR),
                new User (10,"tvSodFNrsQy0", "Asoka", TypeUser.STUDENT),
                new User (11 ,"SLTSeN8scOF", "It", TypeUser.STUDENT)
        )));
    }

    @Test
    public void get_user_by_id() {
        User userGetted = userRepository.findById(1);
        User userExpected = new User(1, "lwrTSIFX", "Brita", TypeUser.ADMINISTRATOR);
        assertThat(userGetted, CoreMatchers.is(userExpected));
    }

    @After
    public void tearDown() throws Exception {

        // Remove H2 files -- https://stackoverflow.com/a/51809831/1121497
        final Statement s = dataSourse.getConnection().createStatement();
        s.execute("drop all objects delete files"); // "shutdown" is also enough for mem db
    }
}