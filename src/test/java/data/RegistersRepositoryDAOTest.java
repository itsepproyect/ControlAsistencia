package data;

import models.Registro;
import models.TypeUser;
import models.User;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import services.UserServices;

import javax.sql.DataSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

public class RegistersRepositoryDAOTest {
    DataSource dataSource;
    JdbcTemplate jdbcTemplate;
    RegistersRepositoryDAO registersRepository;


    @Before
    public void setUp() throws Exception {
        dataSource = new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");

        ScriptUtils.executeSqlScript(
                dataSource.getConnection(), new ClassPathResource("sql-scripts/registers-test-data.sql"));

        jdbcTemplate = new JdbcTemplate(dataSource);

        registersRepository = new RegistersRepositoryDAO(jdbcTemplate);
    }

    @Test
    public void get_all_registers() throws SQLException {
        Collection<Registro> registros = registersRepository.findAllRegisters();

        List<Registro> registroExpected = new ArrayList<>(Arrays.asList(
                new Registro(Timestamp.valueOf("2023-04-26 17:35:01.96327"), 102, 1),
                new Registro(Timestamp.valueOf("2023-04-26 18:29:08.838561"), 23, 2),
                new Registro(Timestamp.valueOf("2023-04-26 18:48:21.199837"), 102, 3),
                new Registro(Timestamp.valueOf("2023-04-26 19:16:06.748825"), 44, 4),
                new Registro(Timestamp.valueOf("2023-04-26 19:16:59.045552"), 10, 5),
                new Registro(Timestamp.valueOf("2023-04-27 20:35:59.045552"), 102, 6),
                new Registro(Timestamp.valueOf("2023-04-28 07:15:59.045552"), 102, 7)));

        assertThat(registros, CoreMatchers.is(registroExpected));
    }

    @Test
    public void get_registers_by_userlogged() {
        Collection<Registro> registrosFromDB = registersRepository.findRegistersByIdUser(102);

        List<Integer> registrosFromDBIds = registrosFromDB.stream().map(registro -> registro.getId()).collect(Collectors.toList());

        assertThat(registrosFromDBIds, CoreMatchers.is(Arrays.asList(1,3,6,7)));
    }

    @After
    public void tearDown() throws Exception {

        // Remove H2 files -- https://stackoverflow.com/a/51809831/1121497
        final Statement s = dataSource.getConnection().createStatement();
        s.execute("drop all objects delete files"); // "shutdown" is also enough for mem db
    }
}