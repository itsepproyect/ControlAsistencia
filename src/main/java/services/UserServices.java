package services;

import UI.UIStudent;
import data.RegistersRepositoryDAO;
import data.UserRepositoryDAO;
import models.Registro;
import models.TypeUser;
import models.User;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Scanner;
import java.util.stream.Collectors;

public class UserServices {

    public static User userLogged;

    /*--------------------Dependencias--------------------*/

    public UserServices(UserRepositoryDAO userRepositoryDAO, RegistersRepositoryDAO registersRepositoryDAO) {
        this.userRepositoryDAO = userRepositoryDAO;
        this.registersRepositoryDAO = registersRepositoryDAO;
    }

    private UserRepositoryDAO userRepositoryDAO;
    private RegistersRepositoryDAO registersRepositoryDAO;


    public static User createUserLogging() {
        int userId = 0;
        String userPassword = "";

        Scanner sc = new Scanner(System.in);

        System.out.println("Login");
        System.out.println("Ingrese el id de usuario");
        // Aqui ver si inplementamos algo para obtener el nombre del usuario y mostrarlo.
        userId = sc.nextInt();
        sc.nextLine();
        System.out.println("Ingrese su contraseña");
        userPassword = sc.next();

        return new User(userId, userPassword);
    }

    public boolean loginUser(User userLoging){
        userLogged = userRepositoryDAO.findById(userLoging.getId());
        //Primero compobamos si el usuario existe y si la contraseña funcio
        if (userLogged != null && userLogged.getPassword().equals(userLoging.getPassword())){
            //Aqui se envia todo el objeto ya que en el check login voy a incluir el checear la contraseña
            return true;
        }
        else {
            return false;
        }
    }

    public User getUserById (int user_id){
        return userRepositoryDAO.findById(user_id);
    }
    public Collection<User> getUsersByTypeUser(TypeUser typeUser){
        return userRepositoryDAO.findAll().stream().filter(user -> user.getTypeUser() == typeUser).collect(Collectors.toList());
    }


    public boolean registrar(User userLoging) {
        if (userLogged != null && userLogged.getPassword().equals(userLoging.getPassword())){
            registersRepositoryDAO.saveRegister(userLogged);
            return true;
        }
        else {
            return false;
        }
    }

    public Collection<Registro> getMyRegisters(){
        return registersRepositoryDAO.findRegistersByIdUser(userLogged.getId());
    }

    public String showMyRegisters(Collection<Registro> registros){
        return registros.toString();
    }

    public void showMenusByLevelAccess(User userLogged) throws SQLException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        System.out.println(userLogged.getName());
            System.out.println("Bienvenido " + userLogged.getName());
            switch (userLogged.getTypeUser()) {
                case STUDENT:
                    UIStudent uiStudent = new UIStudent();
                    uiStudent.showStudentMenu(userLogged);
                    System.out.println("El usuario " + userLogged.getName() + " tiene un nivel de acceso " + userLogged.getTypeUser() + " asi que es un Estudiante");
                    break;
                case TEACHER:
                    //showTeacherMenu();
                    System.out.println("El usuario " + userLogged.getName() + " tiene un nivel de acceso " + userLogged.getTypeUser() + " asi que es un Profesor");
                    break;
                case ADMINISTRATOR:
                    //showAdminMenu();
                    System.out.println("El usuario " + userLogged.getName() + " tiene un nivel de acceso " + userLogged.getTypeUser() + " asi que es un administrador");
                    break;
            }
    }

}
