import UI.UIUser;

import java.sql.SQLException;

public class Main {
    static UIUser uiUser = new UIUser();
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        uiUser.showMainMenu();
    }
}
