package UI;

import DAO.DBactions;
import data.RegistersRepositoryDAO;
import data.UserRepositoryDAO;
import models.User;
import org.springframework.jdbc.core.JdbcTemplate;
import services.UserServices;

import java.sql.SQLException;
import java.util.Scanner;


public class UIUser {

    //Todo esto es para inyectar adecuadamente las dependencias y que se puedan hacer las chingadas pruebas sin fallos
    DBactions dBactions = new DBactions();
    JdbcTemplate jdbcTemplate = dBactions.getJdbcTemplate();
    UserRepositoryDAO userRepositoryDAO = new UserRepositoryDAO(jdbcTemplate);
    RegistersRepositoryDAO registersRepositoryDAO = new RegistersRepositoryDAO(jdbcTemplate);
    UserServices userServices = new UserServices(
            userRepositoryDAO, registersRepositoryDAO
    );

    public void showMainMenu() throws SQLException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        int resuser = 1;
        boolean continuar = true;

        while(continuar) {
            System.out.println("Bienvenido que desea hacer?");
            System.out.println("1 Iniciar sesion");
            System.out.println("0 Salir");

            resuser = sc.nextInt();

            switch (resuser) {
                case 0:
                    continuar = false;
                    break;
                case 1:
                    uILogin();
                    break;
                default:
                    System.out.println("Accion invalida we");
            }
        };
    }

    public void uILogin() throws SQLException, ClassNotFoundException {
        //Ahora creamos una entidad userLogged
        User userLogging = UserServices.createUserLogging();
        //Una vez creado el userLogged se procede a intentar iniciar sesion
        if (userServices.loginUser(userLogging)) {
            User userLogged = userServices.getUserById(userLogging.getId());
            userServices.showMenusByLevelAccess(userLogged);
        }
        else {
            System.out.println("El usuario o constraseña es incorrecto");
        }
    }

    public void showRegisterAttendanceMenu() throws SQLException, ClassNotFoundException {
        User userRegistring = UserServices.userLogged;
        boolean registro_Status = userServices.registrar(userRegistring);

        if (registro_Status){
            System.out.println("Registro completado");
        }
        else {
            Scanner sc = new Scanner(System.in);
            int resuser;

            System.out.println("Registro fallido");
            System.out.println("1 Reintentar");
            System.out.println("2 Menu principal");
            resuser = sc.nextInt();
            switch (resuser){
                case 1:
                    showRegisterAttendanceMenu();
                    break;
                case 2:
                    //Creo que si no pongo nada me regresa al inicio de todo
                    break;
                default:
                    System.out.println("Opcion invalida");

            }
        }

    }

}
