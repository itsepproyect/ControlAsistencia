package UI;

import models.Registro;
import models.User;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Scanner;

public class UIStudent {
    UIUser uiUser = new UIUser();
    public void showStudentMenu(User userLogged) throws SQLException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        boolean continuar = true;

        System.out.println("El usuario " + userLogged.getName() + " tiene un nivel de acceso " + userLogged.getTypeUser() + " asi que es un Estudiante");
        while (continuar){
        System.out.println("Que desea hacer?");
        System.out.println("1. Registrar mi asistencia");
        System.out.println("2. Ver mis asistencias");
        System.out.println("0. Salir");
        int userReponse = sc.nextInt();
        switch (userReponse){
            case 0:
                continuar =false;
                uiUser.showMainMenu();
                break;
            case 1:
                uiUser.showRegisterAttendanceMenu();
                break;
            case 2:
                Collection<Registro> myRegisters = userLogged.getMyRegisters();
                System.out.println(userLogged.showMyRegisters(myRegisters));
                break;
            default:
                System.out.println("Accion de Estudiante no valida");
        }
    }
    }
}
