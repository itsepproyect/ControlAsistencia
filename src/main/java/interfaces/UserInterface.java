package interfaces;

import models.Registro;
import models.TypeUser;
import models.User;

import java.util.Collection;

public interface UserInterface {
    Collection<Registro> getMyRegisters();
    public String showMyRegisters(Collection<Registro> registros);
}
