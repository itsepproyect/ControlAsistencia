package models;

import java.sql.Timestamp;
import java.util.Objects;

public class Registro {
    Timestamp timestamp;
    int id_user;
    Integer id;

    public Registro(Timestamp timestamp, int id_user){
        this(timestamp, id_user, null);
    }

    public Registro(Timestamp timestamp, int id_user, Integer id) {
        this.timestamp = timestamp;
        this.id_user = id_user;
        this.id = id;
    }

    @Override
    public String toString() {
        return "\nRegistro [timestamp=" + timestamp + ", id_user=" + id_user + ", id=" + id + "]\n";
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public int getId_user() {
        return id_user;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Registro registro = (Registro) o;
        return id_user == registro.id_user && Objects.equals(timestamp, registro.timestamp) && Objects.equals(id, registro.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, id_user, id);
    }
}
