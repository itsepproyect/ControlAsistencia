package models;

import DAO.DBactions;
import data.RegistersRepositoryDAO;
import data.UserRepositoryDAO;
import interfaces.UserInterface;
import org.springframework.jdbc.core.JdbcTemplate;
import services.UserServices;

import java.util.Collection;
import java.util.Objects;

public class User implements UserInterface {
    private Integer id;
    private String password;
    private String name;
    private TypeUser typeUser;
    private UserServices userServices;

    DBactions dBactions = new DBactions();
    JdbcTemplate jdbcTemplate = dBactions.getJdbcTemplate();
    UserRepositoryDAO userRepositoryDAO = new UserRepositoryDAO(jdbcTemplate);
    RegistersRepositoryDAO registersRepositoryDAO = new RegistersRepositoryDAO(jdbcTemplate);

    public User(String password, String name, TypeUser typeUser){
        this(null, password, name, typeUser);
    }

    public User(Integer id, String password, String name, TypeUser typeUser) {

        this.id = id;
        this.password = password;
        this.name = name;
        this.typeUser = typeUser;
        this.userServices = new UserServices(
                userRepositoryDAO, registersRepositoryDAO
        );
    }

    public User(int id, String password) {
        this.id = id;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeUser getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(TypeUser typeUser) {
        this.typeUser = typeUser;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", typeUser=" + typeUser +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(password, user.password) && Objects.equals(name, user.name) && typeUser == user.typeUser;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, password, name, typeUser);
    }

    @Override
    public Collection<Registro> getMyRegisters() {
        return userServices.getMyRegisters();
    }

    @Override
    public String showMyRegisters(Collection<Registro> registros) {
        return userServices.showMyRegisters(registros);
    }


}
