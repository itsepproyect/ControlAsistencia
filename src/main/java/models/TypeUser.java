package models;

public enum TypeUser {
    STUDENT, TEACHER, ADMINISTRATOR
}
