package data;

import models.User;

import java.util.Collection;

public interface UserRepository {
    User findById(long id);
    Collection<User> findAll();
    void saveOrUpdate(User user);
}
