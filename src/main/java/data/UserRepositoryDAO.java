package data;

import java.sql.ResultSet;
import java.sql.SQLException;

import models.TypeUser;
import models.User;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;

public class UserRepositoryDAO implements UserRepository{
    private JdbcTemplate jdbcTemplate;
    public UserRepositoryDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    @Override
    public User findById(long id) {
        Object[] args = {id};

        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE id = ?", args, usersMapper);
    }

    @Override
    public Collection<User> findAll() {
        return jdbcTemplate.query("SELECT * FROM users", usersMapper);
    }

    @Override
    public void saveOrUpdate(User user) {
        jdbcTemplate.update("INSERT INTO users (password, name, typeuser) VALUES (?, ?, ?)", user.getPassword(), user.getName(), user.getTypeUser());
    }

    private static RowMapper<User> usersMapper = new RowMapper<User>() {
        @Override
        //Creacion de usuario a partir de la base de datos
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new User(
                    rs.getInt("id"),
                    rs.getString("password"),
                    rs.getString("name"),
                    TypeUser.valueOf(rs.getString("typeuser")));
        }
    };
}