package data;

import models.Registro;
import models.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

public class RegistersRepositoryDAO implements RegistersRepository {
    private JdbcTemplate jdbcTemplate;

    public RegistersRepositoryDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Collection<Registro> findAllRegisters() {
        Collection<Registro> allRegisters = jdbcTemplate.query("SELECT * FROM registros;", registerMapper);
        return allRegisters;
    }

    @Override
    public Collection<Registro> findRegistersByIdUser(long idUser) {
        Object[] args = {idUser};
        Collection<Registro> registersById = jdbcTemplate.query("SELECT * FROM registros WHERE id_user = ?", args, registerMapper);
        return registersById;
    }


    @Override
    public void saveRegister(User user) {
        Timestamp timestamp;

        jdbcTemplate.query("INSERT INTO registros (registro, id_user) VALUES (CURRENT_TIMESTAMP, ?) RETURNING CURRENT_TIMESTAMP", new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                // Extrae la marca de tiempo devuelta por la consulta
                Timestamp timestamp = rs.getTimestamp(1);
                // Haz algo con la marca de tiempo, como imprimirlo en la consola
                System.out.println("Marca de tiempo insertada: " + timestamp);
            }
        }, user.getId());

    }

    private static RowMapper<Registro> registerMapper = new RowMapper<Registro>() {
        @Override
        public Registro mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Registro(
                    rs.getTimestamp("registro"),
                    rs.getInt("id_user"),
                    rs.getInt("id"));
        }
    };

}
