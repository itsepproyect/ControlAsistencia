package data;

import models.Registro;
import models.User;

import java.util.Collection;

public interface RegistersRepository {
    Collection<Registro> findRegistersByIdUser(long id);
    Collection<Registro> findAllRegisters();
    void saveRegister(User user);
}
